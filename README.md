# Funko Rocks ShoppingCart #

This website serves the purpose of showing development and frontend skills of this Developer. 

### Sumary ###

* Overview
* Technology
* Deployment
* Developer

### Overview ###

This website solves the problem:

* As a shopper, I want to add and remove products from my shopping cart and see what I already have
  in it.
  
And aims to show these specific functionalities:
1. I can add products to my basket
2. I can remove products from my basket
3. I can see which products are in my basket
4. I can see costs for the items in my basket
5. A subtotal displays and gets updated whenever I make a changes to my cart

This website is public available in [http://35.198.3.142](http://35.198.3.142) and was deployed using Google Cloud 
Compute Engine (an IaaS service) with a Nginx server pre-installed.

The application is decoupled, so the server and the website exists in different machines and hosts. Also manages the use 
of one ShoppingCart per user session per browser.

### Technology ###

The website was made using the KnockoutJS framework to handle the bindings and data iteration between the HTML and 
JavaScript. It also uses some features from HTML5 and CSS3.
For the interaction with the server was used Jquery to handle the requisitions and responses.

And as mentioned above, the website was deployed in a Nginx server. 

### Deployment ###

Steps for the deployment, considering a linux OS:
1. Clone this repo for your machine/server
2. Copy the directory cloned to /var/www/
3. Creates the config file for Nginx in /etc/nginx/sites-available
4. Creates a link to the config file created in the /etc/nginx/sites-enabled
5. Restarts Nginx and the website will be running.

### Developer ###

* Name: Eduardo Sausen Mallmann
* email: mallmann.edu@gmail.com
* Phone: +55 (48) 9 8819-2784 
* City: Florianópolis
* State: Santa Catarina
* Country: Brazil