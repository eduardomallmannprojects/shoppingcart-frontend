$(function () {

    /**
     * Method to create a Product object.
     *
     * @param id unique identifier of the product
     * @param name product name
     * @param image url address of the image
     * @param price value of one item
     * @param quantity the quantity defined by the buyer
     * @constructor creates a new Product object.
     */
    function Product(id, name, image, price, quantity) {
        let self = this;
        self.id = ko.observable(id);
        self.name = ko.observable(name);
        self.image = ko.observable(image);
        self.price = ko.observable(price);
        self.quantity = ko.observable(quantity);

        self.formattedPrice = ko.computed(function () {
            return price ? "$ " + price.toFixed(2) : "-";
        });
    }

    /**
     * Method that creates the CommerceItem object to be used inside the ShoppingCart.
     *
     * @param id session identifier of the object
     * @param product product name
     * @param quantity quantity desired by the buyer
     * @param amount total value of the item (price * quantity)
     * @constructor creates a new CommerceItem object
     */
    function CommerceItem(id, product, quantity, amount) {
        let self = this;
        self.id = ko.observable(id);
        self.product = ko.observable(product);
        self.quantity = ko.observable(quantity);
        self.amount = ko.computed(function () {
            return amount ? "$ " + amount.toFixed(2) : "-";
        });
    }

    /**
     * Method that creates the ShoppingCart object.
     *
     * @param items list of CommerceItem objects
     * @param amount total amount of the items
     * @constructor creates a new ShoppingCart object
     */
    function ShoppingCart(items, amount) {
        let self = this;
        self.items = ko.observableArray(items);
        self.amount = ko.observable(amount);

        self.formattedPrice = ko.computed(function () {
            return amount ? "$ " + amount.toFixed(2) : "$ 0.00";
        });
    }

    /**
     * ViewModel of the application, responsible for the interactions between server and browser.
     *
     * @constructor creates the ViewModel
     */
    function ShoppingCartViewModel() {
        let self = this;
        let url = "http://35.199.115.126";
        let productsURL = url + "/products";
        let shoppingcartURL = url + "/shoppingcart";
        let commerceItemURL = url + "/shoppingcart/items";

        /**
         * Product list to be presented in dom.
         * @type {observableArray}
         */
        self.products = ko.observableArray([]);

        /**
         * Method to call products objects.
         */
        self.getProducts = function () {
            $.ajax({
                url: productsURL,
                method: 'GET',
                cache: false,
                xhrFields: {
                    withCredentials: true
                },
                success: function (result) {
                    $.each(result, function (key, val) {
                        self.products.push(new Product(val.id, val.name, val.image, val.price, 0))
                    })
                },
                error: function (err) {
                    console.log(err.responseJSON);
                }
            });
        }();


        /**
         * Method to retreive one element from products list.
         */
        self.getOneProduct = function (productId) {
            let productList = ko.toJS(self.products);
            return ko.utils.arrayFilter(productList, function (prod) {
                return prod.id === productId;
            });
        };

        /**
         * Shopping cart object.
         * @type {observable}
         */
        self.shoppingcart = ko.observable();

        /**
         * Method which gets the shoppingcart data.
         */
        self.getShoppingCart = function () {
            $.ajax({
                url: shoppingcartURL,
                method: 'GET',
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                cache: false,
                success: function (result) {
                    if (result.items.length > 0) {
                        let items = [];
                        result.items.forEach(function (item) {
                            let product = ko.toJS(self.getOneProduct(item.product_id))[0];
                            let commerceItem = new CommerceItem(item.id, product.name, item.quantity, item.amount);
                            items.push(commerceItem);
                        });
                        self.shoppingcart(new ShoppingCart(items, result.amount));
                        self.getCommerceItems();
                        self.getSCAmount();
                        self.getVisibility();
                    } else {
                        self.shoppingcart(new ShoppingCart([], 0));
                        self.getCommerceItems();
                        self.getSCAmount();
                        self.getVisibility();
                    }
                },
                error: function (err) {
                    console.log(err.responseJSON);
                }
            });
        };
        self.getShoppingCart();

        /**
         * Total amount of the shoppingcart.
         */
        self.amount = ko.observable();

        /**
         * Method which gets the amount from the shoppingcart.
         */
        self.getSCAmount = function () {
            let cart = ko.toJS(self.shoppingcart);
            self.amount(cart.formattedPrice);
        };

        /**
         * CommerceItems from shoppingcart.
         * @type {observable}
         */
        self.commerceItems = ko.observableArray([]);

        /**
         * Method which convert shoppingcart items in commerceItems.
         */
        self.getCommerceItems = function () {
            let cart = ko.toJS(self.shoppingcart);
            let items = [];
            cart.items.forEach(function (item) {
                items.push(item);
            });
            self.commerceItems(items);
        };

        /**
         * Informs if the shoppingcart will be visible.
         */
        self.visible = ko.observable();

        /**
         * Method that verifies the visibility of the shoppingcart.
         */
        self.getVisibility = function () {
            let items = ko.toJS(self.shoppingcart).items;
            self.visible(items.length !== 0);
        };

        /**
         * Method which add new item to shoppingcart
         * @param product the product to be added
         */
        self.addItem = function (product) {
            if (ko.toJS(product).quantity > 0) {
                $.ajax({
                    url: commerceItemURL + "?" + $.param({
                        product_id: product.id,
                        quantity: product.quantity
                    }),
                    method: 'POST',
                    crossDomain: true,
                    cache: false,
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (result) {
                        self.getShoppingCart();
                    },
                    error: function (err) {
                        console.log(err.responseJSON);
                    }
                });
            }
        };

        /**
         * Method which removes a item from shoppingcart
         * @param item item to be removed
         */
        self.removeItem = function (item) {
            $.ajax({
                url: commerceItemURL + "/" + item.id,
                method: 'DELETE',
                crossDomain: true,
                cache: false,
                xhrFields: {
                    withCredentials: true
                },
                success: function (result) {
                    self.getShoppingCart();
                },
                error: function (err) {
                    console.log(err.responseJSON);
                }
            });
        }

    }

    ko.applyBindings(new ShoppingCartViewModel());
});